﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlyWarCG
{
    public static class CharacterSheetFactory
    {
        /// <summary>
        /// Generate a generic character.
        /// </summary>
        /// <returns>A new character sheet.</returns>
        public static CharacterSheet GenerateCharacter()
        {
            CharacterSheet character = new CharacterSheet();
            character.Skills = new List<Skill>();
            character.Skills.AddRange(Skill.GetAllSkills(character));
            DieRollerUtility utility = new DieRollerUtility();
            utility.RollCharacteristicStats(character);
            character.CharacterName = "Character Name";
            character.Regiment = "Regiment";
            character.Demeanour = "Demeanour";
            character.PlayerName = "Player Name";
            character.Speciality = "Specialty";
            character.Description = "Description";

            character.WeaponSkill = 20 + utility.Roll3D10DiePick2Highest();
            character.BallisticSkill = 20 + utility.Roll3D10DiePick2Highest();
            character.Strength = 20 + utility.Roll3D10DiePick2Highest();
            character.Toughness = 20 + utility.Roll3D10DiePick2Highest();
            character.Agility = 20 + utility.Roll3D10DiePick2Highest();
            character.Intelligence = 20 + utility.Roll3D10DiePick2Highest();
            character.Perception = 20 + utility.Roll3D10DiePick2Highest();
            character.Willpower = 20 + utility.Roll3D10DiePick2Highest();
            character.Fellowship = 20 + utility.Roll3D10DiePick2Highest();

            return character;
        }
    }
}
