﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlyWarCG
{
    class DieRollerUtility
    {
        protected static Random R = new Random(DateTime.Now.Second);

        /// <summary>
        /// Rolls 3 d10s, picks the two highest and adds them together.
        /// </summary>
        /// <returns></returns>
        public int Roll3D10DiePick2Highest()
        {
            List<int> rolls = new List<int>();
            rolls.Add(R.Next(1, 11));
            rolls.Add(R.Next(1, 11));
            rolls.Add(R.Next(1, 11));

            //sort rolls
            rolls.Sort();

            //display rolls...
            rolls.ForEach(x => Console.WriteLine("Rolled " + x));

            Console.WriteLine("Adding {0} and {1}", rolls[0], rolls[1]);
            return rolls[1] + rolls[2];
        }

        public int Roll2D10()
        {
            return R.Next(1, 11) + R.Next(1, 11);
        }

        /// <summary>
        /// Given a character rolls random characteristic stats
        /// and assigns them to appropriate character slots.
        /// </summary>
        public void RollCharacteristicStats(CharacterSheet character)
        {
            //roll random stats
            character.WeaponSkill = Roll3D10DiePick2Highest();
            character.BallisticSkill = Roll3D10DiePick2Highest();
            character.Strength = Roll3D10DiePick2Highest();
            character.Toughness = Roll3D10DiePick2Highest();
            character.Agility = Roll3D10DiePick2Highest();
            character.Intelligence = Roll3D10DiePick2Highest();
            character.Perception = Roll3D10DiePick2Highest();
            character.Willpower = Roll3D10DiePick2Highest();
            character.Fellowship = Roll3D10DiePick2Highest();
        }
    }
}
