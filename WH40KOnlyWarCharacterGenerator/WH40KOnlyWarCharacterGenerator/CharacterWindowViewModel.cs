﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlyWarCG
{
    public class CharacterWindowViewModel
    {
        public CharacterSheet CharacterSheet { get; set; }

        public CharacterWindowViewModel(CharacterSheet characterSheet)
        {
            CharacterSheet = characterSheet;
        }
    }
}
