﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlyWarCG
{
    public enum WeaponClass
    {
        Pistol,
        Basic,
        Heavy,
        Vehicle,
        Thrown,
        Melee,
        Grenade,
        Launcher,
        VehicleMelee
    }
}
