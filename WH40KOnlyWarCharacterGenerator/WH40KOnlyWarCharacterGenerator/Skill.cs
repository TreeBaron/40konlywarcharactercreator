﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace OnlyWarCG
{
    public class Skill : INotifyPropertyChanged
    {
        private bool trained;
        private bool plus10;
        private bool plus20;
        private bool plus30;
        private double skillPass;

        public string Name { get; set; }
        public CharacteristicType Type { get; set; }
        public bool Trained { get => trained; set { trained = value; UpdateSkillPass(); OnPropertyChanged(); } }
        public bool Plus10 { get => plus10; set { plus10 = value; UpdateSkillPass(); OnPropertyChanged(); } }
        public bool Plus20 { get => plus20; set { plus20 = value; UpdateSkillPass(); OnPropertyChanged(); } }
        public bool Plus30 { get => plus30; set { plus30 = value; UpdateSkillPass(); OnPropertyChanged(); } }
        public double SkillPass { get => skillPass; set { skillPass = value; OnPropertyChanged(); } }
        private CharacterSheet Character { get; set; }


        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged([CallerMemberName] string name = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        /// <summary>
        /// Gets the level of this skill (the training) in it.
        /// </summary>
        /// <returns></returns>
        public int GetLevel()
        {
            int level = 0;

            if (Trained)
            {
                level++;
            }

            if (Plus10)
            {
                level++;
            }

            if (Plus20)
            {
                level++;
            }

            if (Plus30)
            {
                level++;
            }

            return level;
        }

        /// <summary>
        /// Gets the amount to treat this skill as
        /// for roll passes.
        /// </summary>
        /// <param name="characteristicValue"></param>
        /// <returns></returns>
        public double GetSkillBonusAmount(CharacterSheet character, CharacteristicType type)
        {
            double characteristicValue = character.GetCharacteristicValue(type);
            int level = GetLevel();
            if (level == 0)
            {
                return characteristicValue / 2.0;
            }
            else if (level == 1)
            {
                return characteristicValue;
            }
            else if (level == 2)
            {
                return characteristicValue + 10;
            }
            else if (level == 3)
            {
                return characteristicValue + 20;
            }
            else if (level == 4)
            {
                return characteristicValue + 30;
            }

            throw new Exception("Level does not exist.");
        }

        /// <summary>
        /// Returns a list of the basic starter skills for use on a character.
        /// </summary>
        /// <returns></returns>
        public static List<Skill> GetAllSkills(CharacterSheet sheet)
        {
            List<Skill> skills = new List<Skill>();

            Skill Acrobatics = new Skill
            {
                Name = "Acrobatics",
                Type = CharacteristicType.Ag
            };
            skills.Add(Acrobatics);

            Skill Athletics = new Skill
            {
                Name = "Athletics",
                Type = CharacteristicType.S
            };
            skills.Add(Athletics);

            Skill Awareness = new Skill
            {
                Name = "Awareness",
                Type = CharacteristicType.Per
            };
            skills.Add(Awareness);

            Skill Charm = new Skill
            {
                Name = "Charm",
                Type = CharacteristicType.Fel
            };
            skills.Add(Charm);

            Skill Command = new Skill
            {
                Name = "Command",
                Type = CharacteristicType.Fel
            };
            skills.Add(Command);

            Skill Commerce = new Skill
            {
                Name = "Commerce",
                Type = CharacteristicType.Int
            };
            skills.Add(Commerce);

            Skill CommonLore = new Skill
            {
                Name = "CommonLore",
                Type = CharacteristicType.Int
            };
            skills.Add(CommonLore);

            Skill Deceive = new Skill
            {
                Name = "Deceive",
                Type = CharacteristicType.Fel
            };
            skills.Add(Deceive);

            Skill Dodge = new Skill
            {
                Name = "Dodge",
                Type = CharacteristicType.Ag
            };
            skills.Add(Dodge);

            Skill ForbiddenLore = new Skill
            {
                Name = "ForbiddenLore",
                Type = CharacteristicType.Int
            };
            skills.Add(ForbiddenLore);

            Skill Inquiry = new Skill
            {
                Name = "Inquiry",
                Type = CharacteristicType.Fel
            };
            skills.Add(Inquiry);

            Skill Interrogation = new Skill
            {
                Name = "Interrogation",
                Type = CharacteristicType.WP
            };
            skills.Add(Interrogation);

            Skill Intimidate = new Skill
            {
                Name = "Intimidate",
                Type = CharacteristicType.S
            };
            skills.Add(Intimidate);

            Skill Linguistics = new Skill
            {
                Name = "Linguistics",
                Type = CharacteristicType.Int
            };
            skills.Add(Linguistics);

            Skill Logic = new Skill
            {
                Name = "Logic",
                Type = CharacteristicType.Int
            };
            skills.Add(Logic);

            Skill Medicae = new Skill
            {
                Name = "Medicae",
                Type = CharacteristicType.Int
            };
            skills.Add(Medicae);

            Skill NavigateSurface = new Skill
            {
                Name = "NavigateSurface",
                Type = CharacteristicType.Int
            };
            skills.Add(NavigateSurface);

            Skill NavigateStellar = new Skill
            {
                Name = "NavigateStellar",
                Type = CharacteristicType.Int
            };
            skills.Add(NavigateStellar);

            Skill NavigateWarp = new Skill
            {
                Name = "NavigateWarp",
                Type = CharacteristicType.Int
            };
            skills.Add(NavigateWarp);

            Skill OperateAeronautica = new Skill
            {
                Name = "OperateAeronautica",
                Type = CharacteristicType.Ag
            };
            skills.Add(OperateAeronautica);

            Skill OperateSurface = new Skill
            {
                Name = "OperateSurface",
                Type = CharacteristicType.Ag
            };
            skills.Add(OperateSurface);

            Skill OperateVoidShip = new Skill
            {
                Name = "OperateVoidShip",
                Type = CharacteristicType.Ag
            };
            skills.Add(OperateVoidShip);

            Skill Parry = new Skill
            {
                Name = "Parry",
                Type = CharacteristicType.WS
            };
            skills.Add(Parry);

            Skill Psyniscience = new Skill
            {
                Name = "Psyniscience",
                Type = CharacteristicType.Per
            };
            skills.Add(Psyniscience);

            Skill ScholasticLore = new Skill
            {
                Name = "ScholasticLore",
                Type = CharacteristicType.Int
            };
            skills.Add(ScholasticLore);

            Skill Scrutiny = new Skill
            {
                Name = "Scrutiny",
                Type = CharacteristicType.Per
            };
            skills.Add(Scrutiny);

            Skill Security = new Skill
            {
                Name = "Security",
                Type = CharacteristicType.Int
            };
            skills.Add(Security);

            Skill SleightOfHand = new Skill
            {
                Name = "SleightOfHand",
                Type = CharacteristicType.Ag
            };
            skills.Add(SleightOfHand);

            Skill Stealth = new Skill
            {
                Name = "Stealth",
                Type = CharacteristicType.Ag
            };
            skills.Add(Stealth);

            Skill Survival = new Skill
            {
                Name = "Survival",
                Type = CharacteristicType.Per
            };
            skills.Add(Survival);

            Skill TechUse = new Skill
            {
                Name = "TechUse",
                Type = CharacteristicType.Int
            };
            skills.Add(TechUse);

            Skill Trade = new Skill
            {
                Name = "Trade",
                Type = CharacteristicType.Int
            };
            skills.Add(Trade);

            if (sheet != null)
            {
                skills.ForEach(x => x.Character = sheet);
            }

            return skills;
        }

        /// <summary>
        /// Updates the skill pass for the user to use on rolls
        /// </summary>
        public void UpdateSkillPass()
        {
            if (Character == null) return;
            SkillPass = GetSkillBonusAmount(Character, this.Type);
        }

    }
}
