﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace OnlyWarCG
{
    public class CharacterSheet : INotifyPropertyChanged
    {
        private string characterName;
        private string regiment;
        private string demeanour;
        private string playerName;
        private string speciality;
        private string description;
        private string talentsAndTraits;
        private int weaponSkill;
        private int ballisticSkill;
        private int strength;
        private int toughness;
        private int agility;
        private int intelligence;
        private int perception;
        private int willpower;
        private int fellowship;
        private List<Skill> skills;

        public string CharacterName
        {
            get
            {
                return characterName;
            }
            set
            {
                characterName = value;
                OnPropertyChanged();
            }
        }

        public string Regiment { get => regiment; set { regiment = value; OnPropertyChanged(); } }

        public string Demeanour { get => demeanour; set { demeanour = value; OnPropertyChanged(); } }

        public string PlayerName { get => playerName; set { playerName = value; OnPropertyChanged(); } }

        public string Speciality { get => speciality; set { speciality = value; OnPropertyChanged(); } }

        public string Description { get => description; set { description = value; OnPropertyChanged(); } }

        public string TalentsAndTraits { get => talentsAndTraits; set { talentsAndTraits = value; OnPropertyChanged(); } }

        public int WeaponSkill { get => weaponSkill; set { weaponSkill = value; OnPropertyChanged(); } }

        public int BallisticSkill { get => ballisticSkill; set { ballisticSkill = value; OnPropertyChanged(); } }

        public int Strength { get => strength; set { strength = value; OnPropertyChanged(); } }

        public int Toughness { get => toughness; set { toughness = value; OnPropertyChanged(); } }

        public int Agility { get => agility; set { agility = value; OnPropertyChanged(); } }

        public int Intelligence { get => intelligence; set { intelligence = value; OnPropertyChanged(); } }

        public int Perception { get => perception; set { perception = value; OnPropertyChanged(); } }

        public int Willpower { get => willpower; set { willpower = value; OnPropertyChanged(); } }

        public int Fellowship { get => fellowship; set { fellowship = value; OnPropertyChanged(); } }

        public List<Skill> Skills { get => skills; set { skills = value; OnPropertyChanged(); } }



        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged([CallerMemberName] string name = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        /// <summary>
        /// Given a characteristic type enum, returns that characteristics integer value.
        /// </summary>
        /// <param name="type">Characteristic type.</param>
        /// <returns></returns>
        public int GetCharacteristicValue(CharacteristicType type)
        {
            switch (type)
            {
                case CharacteristicType.Ag:
                    return this.Agility;
                case CharacteristicType.Fel:
                    return this.Fellowship;
                case CharacteristicType.Int:
                    return this.Intelligence;
                case CharacteristicType.Per:
                    return this.Perception;
                case CharacteristicType.S:
                    return this.Strength;
                case CharacteristicType.WP:
                    return this.Willpower;
                case CharacteristicType.WS:
                    return this.WeaponSkill;
            }

            throw new Exception("Type not found.");
        }
    }
}
