﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlyWarCG
{
    public enum CharacteristicType
    {
        Ag,
        S,
        Per,
        Fel,
        Int,
        WP,
        WS
    }

}
