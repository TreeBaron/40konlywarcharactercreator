﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlyWarCG
{
    public class Weapon
    {
        public string Name { get; set; }
        public WeaponClass Class { get; set; }
        public string Damage { get; set; }
        public string Type { get; set; }
        public int Pen { get; set; }
        public int Range { get; set; }
        public string ROF { get; set; }
        public int Clip { get; set; }
        public int RLD { get; set; }
    }
}
